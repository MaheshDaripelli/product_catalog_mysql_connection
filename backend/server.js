// importing express from the express module
import mongoose from "mongoose";
import express from "express";
//importign bodyparser from the body-parser module
import bodyParser from "body-parser";
//Creating an instance of express
const app = express();
//Returns middleware that only parses json
app.use(bodyParser.json());
//Returns middleware that only parses urlencoded bodies
app.use(bodyParser.urlencoded({ extended: true }));

//importing cors
import cors from "cors";
//enable the express server to respond to preflight requests
app.use(
  cors({
    origin: "*",
  })
);
import {
  get_all_products,
  get_product_based_on_id,
  get_all_products_for_count
} from "./path_handlers/get_apis.js";
import {
  create_product,
  login_user_handler,
  create_profile_path_handler,
} from "./path_handlers/post_apis.js";
import { update_product } from "./path_handlers/put_apis.js";
import { delete_product } from "./path_handlers/delete_apis.js";
import {
  validation_for_product_data,
  register_data_validator,
} from "./utils/validation.js";
import { validate_login_params } from "./utils/validation.js";

app.listen(1947, () => {
  console.log("this server is connected to http://localhost:1947");
});

app.get("/products", get_all_products);

app.get("/allproducts", get_all_products_for_count)

/**
 * @path /login/:user_type checks for the type of user to logjn
 * @path "validate_login_params" is the middleware that contains the data that passed from the client(postman)
 * checks for the keys,data_type,not_empty_fields
 * @param login_user_handler checks for the user already existed in the database if present
 * logs succesfully otherwise gives error with status_message and code.
 */
app.post("/login", validate_login_params, login_user_handler);

/**
 * @path /user_register checks for the type of user to register
 * @path "validate_params" is the middleware that contains the data that passed from the client(postman)
 * checks for the keys,data_type,not_empty_fields
 * @param "validate_another" is the middleware that contains the data that passed from the client(postman)
 * checks
 * @param create_profile_path_handler checks for the user already existed in the database if not
 * inserts the data into the database
 */
app.post(
  "/user_register",
  register_data_validator,
  create_profile_path_handler
);

app.post("/product/create", validation_for_product_data, create_product);

app.put("/update", update_product);

app.delete("/product/delete", delete_product);

app.get("/product/:id", get_product_based_on_id);
