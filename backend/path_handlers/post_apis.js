import connection from "../utils/db_connection.js";
import { v4 as uuidv4 } from "uuid";
import { validate_user_login_data } from "../utils/validation.js";
import jwt from "jsonwebtoken";
import dotenv from "dotenv";
dotenv.config();

const create_product = (request, response) => {
  const data = request.body;
  if (data.discount === "") {
    data.discount = 0;
  }
  const id = uuidv4();
  const query = `insert into product_catalog values ('${id}','${data.name}','${data.description}',${data.price},${data.quantity},${data.discount},'${data.merchant_name}')`;
  connection.query(query, (err, result) => {
    if (err) {
      console.log(err);
      response.send(err);
    } else {
      response.send(
        JSON.stringify({
          status_code: 200,
          status_message: "data added successfully",
        })
      );
    }
  });
};

/**
 * @path /login is the path by using this path we can post the user login data and validate it using the register data
 * @param request is the callback parameter that contains the data that passed from client(postman)
 * based on that data server respond
 * @param response is the callback parameter that sends the data based on the request data
 */
const login_user_handler = (request, response) => {
  // login_data is the variable that contains the request body data
  const login_data = request.body;
  const user = { name: login_data.username };
  const jwt_token = jwt.sign(user, process.env.ACCESS_TOKEN_SECRET);
  login_data.jwt_token = jwt_token;
  validate_user_login_data(login_data, response);
};

const create_profile_path_handler = (request, response) => {
  const data = request.body;
  const id = uuidv4();
  const query = `insert into product_register values ('${id}','${data.username}','${data.password}','${data.email}')`;
  connection.query(query, (err, result) => {
    if (err) {
      response.send(
        JSON.stringify({
          status_code: 401,
          status_message: "username or email already taken",
        })
      );
    } else {
      response.send(
        JSON.stringify({
          status_code: 200,
          status_message: "Registered successfully",
        })
      );
    }
  });
};

export { create_product, login_user_handler, create_profile_path_handler };
