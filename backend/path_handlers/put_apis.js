import connection from "../utils/db_connection.js";

const update_product = async (request, response) => {
  const data = request.body;
  if (data.discount === "") {
    data.discount = 0;
  }
  const query = `update product_catalog set price = ${data.price}, name = '${data.name}', discount = '${data.discount}', merchant_name = '${data.merchantname}', quantity = '${data.quantity}', description = '${data.description}' where id = '${data.id}'`;
  connection.query(query, (err, result) => {
    if (err) {
      response.send(err);
    } else {
      response.send(
        JSON.stringify({
          status_code: 200,
          status_message: "data updated successfully",
        })
      );
    }
  });
};

export { update_product };
