import connection from "../utils/db_connection.js";

const get_all_products = (request, response) => {
  const order = request.headers.order;
  const search = request.headers.search;
  const limit = request.headers.limit;
  const offset = request.headers.offset;
  let sub_query = `limit ${limit} offset ${offset}`;

  const query =
    `select * from product_catalog where name LIKE ` +
    connection.escape(`%${search}%`) +
    `order by price ${order} ${sub_query}`;
  connection.query(query, (err, result) => {
    if (err) {
      response.send(response);
    } else {
      response.send(result);
    }
  });
};

const get_product_based_on_id = (request, response) => {
  const { id } = request.params;
  const query = `select * from product_catalog where id = '${id}'`;
  connection.query(query, (err, result) => {
    if (err) {
      response.send(err);
    } else {
      response.send(result);
    }
  });
};

const get_all_products_for_count = (request, response) => {
  const query1 = "select * from product_catalog";
  connection.query(query1, (err, result1) => {
    if (err) {
      console.log("not working", err);
    } else {
      response.send(result1);
    }
  });
};
export {
  get_all_products,
  get_product_based_on_id,
  get_all_products_for_count,
};
