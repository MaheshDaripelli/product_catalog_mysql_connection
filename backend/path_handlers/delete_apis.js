import connection from "../utils/db_connection.js";

const delete_product = (request, response) => {
  const data = request.body;
  const query = `delete from product_catalog where id = '${data.id}'`;
  connection.query(query, (err, result) => {
    if (err) {
      response.send(err);
    } else {
      response.send(
        JSON.stringify({
          status_code: 200,
          status_message: "data deleted successfully",
        })
      );
    }
  });
};
export { delete_product };
