//importing bcrypt
import bcrypt from "bcrypt";
import connection from "./db_connection.js";

/**
 * Helper function to validate name string
 * @param {} login_data
 * @returns if conditions of name satisfies returns param
 * otherwise returns error
 */
function product_data_keys_validation(login_data, response) {
  let param_list = [
    "name",
    "description",
    "price",
    "quantity",
    "merchant_name",
    "discount",
  ];
  let keys = Object.keys(login_data);
  let param = "";
  for (let item of param_list) {
    if (!keys.includes(item)) {
      param = item;
    }
  }
  return param;
}

/**
 * Helper function to validate name string
 * @param  login_data
 * @returns checks for empty fields if not satisfies returns false
 * otherwise it returns true
 */
function empty_product_fields(login_data) {
  const { name, description, price, merchant_name, quantity } = login_data;
  if (
    name === "" ||
    description === "" ||
    merchant_name === "" ||
    price === "" ||
    quantity === ""
  ) {
    return true;
  }
  return false;
}

const validation_for_product_data = (request, response, next) => {
  const data = request.body;
  //initializing empty_fields as empty_login_fields
  const empty_fields = empty_product_fields(data);
  //initializing keys_validation as login_data_keys_validation
  const keys_validations = product_data_keys_validation(data, response);
  if (keys_validations) {
    response.send(
      JSON.stringify({
        status_code: 422,
        status_message: `${keys_validations} is required`,
      })
    );
  } else if (empty_fields) {
    response.send(
      JSON.stringify({
        status_code: 401,
        status_message: "All fields should be filled",
      })
    );
  } else {
    next();
  }
};

/**
 *
 * @param  name
 * @returns if name credentials satisfies returns status_code 200
 * checks for the datatype,length if not satisfies returns error status code
 * with message
 */
const _name_helper = (name) => {
  const returnObject = {};

  if (typeof name != "string") {
    returnObject.status_message = "name should be of string type";
    returnObject.status_code = 400;
    return returnObject;
  }

  if (name.length < 4) {
    returnObject.status_message =
      "name length should be at least 4 characters long";
    returnObject.status_code = 400;
    return returnObject;
  }

  if (name.length > 25) {
    returnObject.status_message = "name length shouldn't be 25 characters long";
    returnObject.status_code = 400;
    return returnObject;
  }

  if (!name.match(/^[a-zA-Z ]+$/)) {
    returnObject.status_message = "name should be of the correct format";
    returnObject.status_code = 400;
    return returnObject;
  }

  return { status_code: 200 };
};

/**
 * Helper function to validate name string
 * @param  password
 * @returns if password credentials satisfies returns status_code 200
 * checks for the datatype,length if not satisfies returns error status code
 * with message as objects
 */
const _password_helper = (password) => {
  const returnObject = {};
  // is it a string
  if (typeof password != "string") {
    returnObject.status_message = "password should be of string type";
    returnObject.status_code = 400;
    return returnObject;
  }
  if (password.length < 10) {
    returnObject.status_message =
      "password length should be at least 10 characters long";
    returnObject.status_code = 400;
    return returnObject;
  }
  if (password.length > 20) {
    returnObject.status_message = "password length should be a max of 20";
    returnObject.status_code = 400;
    return returnObject;
  }
  if (
    !password.match(/^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{10,20}$/)
  ) {
    returnObject.status_message =
      "password should contains atleast one special char and number and captial letter";
    returnObject.status_code = 400;
    return returnObject;
  }
  return { status_code: 200 };
};

/**
 * Helper function to validate name string
 * @param  login_data
 * @returns checks for empty fields if not satisfies returns false
 * otherwise it returns true
 */
function empty_login_fields(login_data) {
  const { email, password } = login_data;
  if (email === "" || password === "") {
    return true;
  }
  return false;
}

/**
 *
 * @param email
 * @returns if email credentials satisfies returns status_code 200
 * checks for the datatype,length if not satisfies returns error status code
 * with message
 */
const _email_helper = (email) => {
  const returnObject = {};
  // is it a string
  if (typeof email != "string") {
    returnObject.status_message = "email should be of string type";
    returnObject.status_code = 400;
    return returnObject;
  }
  if (email.length < 6) {
    returnObject.status_message =
      "email length should be at least 6 characters long";
    returnObject.status_code = 400;
    return returnObject;
  }
  if (email.length > 50) {
    returnObject.status_message = "email length should be a max of 50";
    returnObject.status_code = 400;
    return returnObject;
  }
  if (!email.endsWith("@innominds.com")) {
    returnObject.status_message = "email should be ends with @innominds.com";
    returnObject.status_code = 400;
    return returnObject;
  }

  return { status_code: 200 };
};
/**
 * Helper function to validate name string
 * @param {} login_data
 * @returns if conditions of name satisfies returns param
 * otherwise returns error
 */
function login_data_keys_validation(login_data) {
  let param_list = ["email", "password"];
  let param = "";
  for (let item in login_data) {
    if (!param_list.includes(item)) {
      param = item;
    }
  }
  return param;
}

/**
 * This is a helper middleware function to help with validation of login
 * parameters.
 * @param req   HTTP req object as recieved by Express backend
 * @param res   HTTP response object to be populated by Express backend
 * @param next  next() is a function to be called if this middlware validation is success
 * validate_login_params is a function for validating all the possibilities
 * if all conditions satisfies it goes to next() if not
 * @returns status_code with message
 */
const validate_login_params = (request, response, next) => {
  //initializing login_data as the request.body
  const login_data = request.body;
  //initializing keys_validation as login_data_keys_validation
  const keys_validations = login_data_keys_validation(login_data);
  //initializing empty_fields as empty_login_fields
  const empty_fields = empty_login_fields(login_data);
  //initializing password_validate as _password_helper
  const password_validate = _password_helper(login_data.password);
  //initializing validate_email as _email_helper
  const validate_email = _email_helper(login_data.email);
  if (keys_validations) {
    response.send(
      JSON.stringify({
        status_code: 422,
        status_message: `${keys_validations} is not a valid key`,
      })
    );
  } else if (empty_fields) {
    response.send(
      JSON.stringify({
        status_code: 401,
        status_message: "email and password not be empty",
      })
    );
  } else if (password_validate.status_code !== 200) {
    response.send(JSON.stringify(password_validate));
  } else if (validate_email.status_code !== 200) {
    response.send(JSON.stringify(validate_email));
  } else {
    next();
  }
};

const inserting_user_login_data_into_database = (data) => {
  const query = `insert into login_data values ('${data.id}','${data.email}','${data.jwt_token}')`;
  connection.query(query, (err, result) => {
    if (err) {
      console.log(err);
    }
  });
};

/**
 * validate the user login data based on the user register database data
 * @param login_data that contains the user login data
 * @param response which is the response object that send the response to the client
 * @returns the different response like if email not found or password not correct or login success
 * based on the conditions with validating the user register database data
 */
async function validate_user_login_data(login_data, response) {
  // fetching the user register data from the data base for login validatinn
  const query = `select * from product_register where email = '${login_data.email}'`;
  connection.query(query, async (err, data) => {
    if (err) {
      response.send(err);
    } else {
      // if there any data that matches with the email it goes through the if condition
      if (data.length !== 0) {
        // iterating over the data fetched from the database
        for (let item of data) {
          // validating the password
          // const isPasswordMatch = await bcrypt.compare(
          //   login_data.password,
          //   item.password
          // );
          if (login_data.password === item.password) {
            // if password is valid it sends the response as 200 'success'
            // const x = is_login_before(login_data)
            // console.log(x)
            login_data.status_code = 200;
            login_data.id = data[0].id;
            inserting_user_login_data_into_database(login_data);
            response.send(JSON.stringify(login_data));
            break;
          }
          // if password is invalid it sends the response as password not match
          else {
            const obj = {
              status_code: 401,
              status_message: "invalid password",
            };
            response.send(JSON.stringify(obj));
            break;
          }
        }
      }
      // if fetching data count is empty it sends the response as user not found
      else {
        const obj = {
          status_code: 404,
          status_message: "email not found",
        };
        response.send(JSON.stringify(obj));
      }
    }
  });
}

/**
 *
 * @param request request from the client
 * @param response sends response to the client
 * @param next Callback argument to the middleware function, called "next" by convention.
 * checking params one by one with our required specifications
 * if not returns error messages with statuscode
 * if all params satisfies the condition goes to next middleware
 */
const register_data_validator = (request, response, next) => {
  //destructuring items from the request.body
  const { username, password, email } = request.body;
  const name_validation_result = _name_helper(username);
  const email_validation_email = _email_helper(email);
  const password_validatino_helper = _password_helper(password);
  //console.log(`error obj ${error_object}`);
  if (name_validation_result.status_code !== 200) {
    response.send(JSON.stringify(name_validation_result));
  } else if (email_validation_email.status_code !== 200) {
    response.send(JSON.stringify(email_validation_email));
  } else if (password_validatino_helper.status_code !== 200) {
    response.send(JSON.stringify(password_validatino_helper));
  } else {
    next();
  }
};

export {
  validation_for_product_data,
  validate_login_params,
  validate_user_login_data,
  register_data_validator,
};
