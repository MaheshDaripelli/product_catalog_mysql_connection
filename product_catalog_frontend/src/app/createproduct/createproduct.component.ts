import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-createproduct',
  templateUrl: './createproduct.component.html',
  styleUrls: ['./createproduct.component.css']
})
export class CreateproductComponent implements OnInit {

  // change = (event:any)=>{
  //   console.log(event.target.value)
  // }

  change(event: any) { console.log(event.target.value); }
  // public constructor(name:string) {

  //  }
  async submitingForm(product: any, merchant: any, description: any, price: any, discount: any, quantity: any, event: any) {
    event.preventDefault()
    const bodyData = {
      name: product.value,
      description: description.value,
      price: price.value,
      quantity: quantity.value,
      merchant_name: merchant.value,
      discount: discount.value,
    }
    const url = "http://localhost:1947/product/create"
    const option = {
      method: "POST",
      body: JSON.stringify(bodyData),
      headers: {
        "Content-Type": "application/json",
        "Accept": "application/json",
      },
    };
    const response = await fetch(url, option)
    const data = await response.json()
    if (data.status_code === 200) {
      alert("data added successfully")
      product.value = ""
      description.value = ""
      price.value = ""
      quantity.value = ""
      merchant.value = ""
      discount.value = ""
    }
  }


  ngOnInit(): void {

  }

}
