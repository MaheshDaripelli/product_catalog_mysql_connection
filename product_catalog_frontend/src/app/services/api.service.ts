import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  public check:string = "pagination contains"
  constructor(private http: HttpClient) { }
  
  getProduct(order:string,limit:string,offset:string,orderby:string) {
    // const headers = new HttpHeaders();
    // headers.set('Content-Type', 'application/json; charset=utf-8');
    // headers.set('search', '""');
    // headers.set('order', 'DESC');
    const headers = {
      "Content-Type":"application/json",
      "search":orderby,
      "order":order,
      "limit": limit,
      "offset":offset
    }
    // headers.search = ""
    return this.http.get<any>('http://localhost:1947/products',{headers:headers}).pipe(
      map((res: any) => {
        return res;
      })
    );
  }

  getAllProducts(){
    const headers = {
      "Content-Type":"application/json"
    }
    return this.http.get<any>("http://localhost:1947/allproducts",{headers:headers}).pipe(
      map((res:any)=>{
        return res
      })
    )
  }

  deleteProduct(id:any){
    const body = {
      id:id
    }
    return this.http.delete<any>("http://localhost:1947/product/delete",{body})
  }
}
