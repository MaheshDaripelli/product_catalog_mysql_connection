import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute,Router } from '@angular/router';

@Component({
  selector: 'app-updateproduct',
  templateUrl: './updateproduct.component.html',
  styleUrls: ['./updateproduct.component.css']
})
export class UpdateproductComponent implements OnInit {

  public product: any = history.state.element;
  public name : string = this.product ? this.product.name : "";
  public description : string = this.product ? this.product.description: "";
  public price : string = this.product ? this.product.price : ""
  public discount : string = this.product ? this.product.discount : ""
  public quantity : string = this.product ? this.product.quantity : ""
  public merchant : string = this.product ? this.product.merchant_name : ""
  public id : string = this.product ? this.product.id : ""

  constructor(private router: Router) {

  }
  changeQuantity = (event:any)=>{
    this.quantity = event.target.value
  }
  changeDiscount = (event:any)=>{
    this.discount = event.target.value
  }
  changeDescription = (event:any)=>{
    this.description = event.target.value
  }
  changePrice = (event:any)=>{
    this.price = event.target.value
  }
  changeMerchant = (event:any)=>{
    this.merchant = event.target.value
  }
  changeName = (event:any)=>{
    this.name = event.target.value
  }
  changeId = (event:any)=>{
    this.id = event.target.value
  }
  
  async submitingForm(event: any) {
    event.preventDefault()
    const bodyData = {
      price:this.price,
      description:this.description,
      name:this.name,
      discount:this.discount,
      merchantname:this.merchant,
      quantity:this.quantity,
      id:this.id
    }
    console.log()
    const url = "http://localhost:1947/update"
    const option = {
      method: "PUT",
      body: JSON.stringify(bodyData),
      headers: {
        "Content-Type": "application/json",
        "Accept": "application/json",
      },
    };
    const response = await fetch(url, option)
    console.log(response)
    const data = await response.json()
    console.log(data)
    if (data.status_code === 200) {
      alert("data added successfully")
      this.router.navigate(['/products']);
    }
  }



  ngOnInit(): void {
    console.log(this.product)
    console.log(this.id)
  }

}
