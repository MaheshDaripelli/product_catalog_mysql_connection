import { LoginComponent } from './login/login.component';
import { ModalComponent } from './modal/modal.component';
import { ProductsComponent } from './products/products.component';
import { CreateproductComponent } from './createproduct/createproduct.component';
import { TablePaginationExample } from './home/home.component';
import { HeaderComponent } from './header/header.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UpdateproductComponent } from './updateproduct/updateproduct.component';

const routes: Routes = [
  {path : '' , pathMatch: 'full', redirectTo: 'home'},
  {
    component:HeaderComponent,
    path:"header"
  },
  {
    component:TablePaginationExample,
    path:"home"
  },
  {
    component:CreateproductComponent,
    path:"create_product"
  },
  {
    component:UpdateproductComponent,
    path:"update_product"
  },
  {
    component:ProductsComponent,
    path:"products"
  },
  {
    component:ModalComponent,
    path:"modal"
  },
  {
    component:LoginComponent,
    path:"login"
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
