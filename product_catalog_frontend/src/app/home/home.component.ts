import { AfterViewInit, Component, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { ProductService } from '../services/api.service';

/**
 * @title Table with pagination
 */
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class TablePaginationExample implements AfterViewInit {
  displayedColumns: string[] = ['name', 'description', 'price', 'discount', "merchantname", "quantity", "delete operation", "update operation"];

  public products: any = [];
  constructor(private productService: ProductService) { }
  

  dataSource = new MatTableDataSource(this.products);


  getProducts = async () => {
    const url = "http://localhost:1947/products";
    const option = {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
        order: "DESC",
        search: "",
      },
    };
    const response = await fetch(url, option)
    const data = await response.json()
    console.log(data)
    console.log(response)
    // console.log(data)
  }

  ngOnInit(): void {
    // this.productService.getProduct()
    //   .subscribe(res => {
    //     this.products = res;
    //     console.log("hello",this.products)
    //   })
      // this.doRerender();
      
    //this.getProducts()
  }

  @ViewChild(MatPaginator) paginator: MatPaginator;

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }
}

// export interface PeriodicElement {
//   name: string;
//   position: number;
//   weight: number;
//   symbol: string;
// }

// const getProducts = async () => {
//   const url = "http://localhost:1947/products";
//   const option = {
//     method: "GET",
//     headers: {
//       "Content-Type": "application/json",
//       Accept: "application/json",
//       order: "DESC",
//       search: "",
//     },
//   };
//   const response = await fetch(url, option)
//   const data = await response.json()
//   // return response
//   return data
//   // console.log(data)
//   // console.log(response)
//   // console.log(data)
// }

// let ELEMENT_DATA: any
// ELEMENT_DATA = getProducts()
// console.log("element_data", ELEMENT_DATA)

const ELEMENT_DATA = [
  {position: 1, name: 'Hydrogen', weight: 1.0079, symbol: 'H'},
  {position: 2, name: 'Helium', weight: 4.0026, symbol: 'He'},
  {position: 3, name: 'Lithium', weight: 6.941, symbol: 'Li'},
  {position: 4, name: 'Beryllium', weight: 9.0122, symbol: 'Be'},
  {position: 5, name: 'Boron', weight: 10.811, symbol: 'B'},
  {position: 6, name: 'Carbon', weight: 12.0107, symbol: 'C'},
  {position: 7, name: 'Nitrogen', weight: 14.0067, symbol: 'N'},
  {position: 8, name: 'Oxygen', weight: 15.9994, symbol: 'O'},
  {position: 9, name: 'Fluorine', weight: 18.9984, symbol: 'F'},
  {position: 10, name: 'Neon', weight: 20.1797, symbol: 'Ne'},
  {position: 11, name: 'Sodium', weight: 22.9897, symbol: 'Na'},
  {position: 12, name: 'Magnesium', weight: 24.305, symbol: 'Mg'},
  {position: 13, name: 'Aluminum', weight: 26.9815, symbol: 'Al'},
  {position: 14, name: 'Silicon', weight: 28.0855, symbol: 'Si'},
  {position: 15, name: 'Phosphorus', weight: 30.9738, symbol: 'P'},
  {position: 16, name: 'Sulfur', weight: 32.065, symbol: 'S'},
  {position: 17, name: 'Chlorine', weight: 35.453, symbol: 'Cl'},
  {position: 18, name: 'Argon', weight: 39.948, symbol: 'Ar'},
  {position: 19, name: 'Potassium', weight: 39.0983, symbol: 'K'},
  {position: 20, name: 'Calcium', weight: 40.078, symbol: 'Ca'},
];
