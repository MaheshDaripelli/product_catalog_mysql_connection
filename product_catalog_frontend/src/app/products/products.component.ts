import { Component, OnInit } from '@angular/core';
import { ProductService } from '../services/api.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  public products: any = [];
  public order:string = "ASC"
  public limit :string = "5"
  public activePage : number = 1
  public offset : string = "0"
  public dataLength:number = 0
  public dataCount:number = 0
  public orderby:string = ""
  constructor(private productService: ProductService,private route: ActivatedRoute) { }

  deleteApiCall = (id:any)=>{
    this.productService.deleteProduct(id).subscribe(res=>{
    })
    window.location.reload()
  }

  changeSelect = (event:any)=>{
    this.order = event.target.value
    this.productApiCall()
  }

  ngOnInit(): void {
    this.route.queryParams
      .subscribe(params => {
        console.log(params); // { orderby: "price" }
        if (params['search'] !== undefined){
          this.orderby = params['search'];
        }
        console.log(this.orderby); // price
      }
    );
    this.productApiCall()
    this.productService.getAllProducts().subscribe(res=>{
      this.dataCount = res.length
      this.dataLength = Math.ceil(res.length / parseInt(this.limit))
    })
  }
  selectLimitChange = (event:any)=>{
    this.limit = event.target.value
    this.activePage = 1
    this.offset = JSON.stringify((this.activePage - 1) * parseInt(this.limit))
    this.dataLength = Math.ceil(this.dataCount / parseInt(this.limit))
    this.productApiCall()
  }

  onClickLeftArrow = () => {
    if (this.activePage > 1) {
      this.activePage = this.activePage - 1
      this. offset = JSON.stringify((this.activePage - 1) * parseInt(this.limit))
      this.productApiCall()
    }
  };

  onClickRightArrow = () => {
    if (this.activePage < this.dataLength) {
      this.activePage = this.activePage + 1
      this. offset = JSON.stringify((this.activePage - 1) * parseInt(this.limit))
      this.productApiCall()
    }
  };

  productApiCall = ()=>{
    this.productService.getProduct(this.order,this.limit,this.offset,this.orderby)
      .subscribe(res => {
        this.products = res;
      })
  }

}
