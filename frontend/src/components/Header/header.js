import "./header.css";
import ProductContext from "../../Context/context";
import { Component } from "react";
import { Link, withRouter } from "react-router-dom";
import Cookies from "js-cookie";

class Header extends Component {
  state = {
    search: "",
  };
  changeSearch = (event) => {
    this.setState({ search: event.target.value });
  };
  logoutClick = () => {
    const { history } = this.props;
    Cookies.remove("jwt_token");
    history.replace("/signin");
  };
  render() {
    return (
      <ProductContext.Consumer>
        {(value) => {
          const { updateSearch } = value;
          const submitingSearchForm = (event) => {
            event.preventDefault();
            const { search } = this.state;
            updateSearch(search);
          };
          return (
            <div>
              <header id="site-header">
                <div id="site-header-container">
                  <div id="site-header-logo">
                    <img
                      src="https://github.com/taviskaron/t-div-headers/blob/main/img/logo3.png?raw=true"
                      alt=""
                    />
                  </div>
                  <input
                    type="checkbox"
                    className="toggleSideMenu"
                    id="toggleSideMenu"
                    autoComplete="off"
                  />
                  <label htmlFor="toggleSideMenu" className="hamburger-icon">
                    <div className="hamburger-menu-line diagonal-1" />
                    <div className="hamburger-menu-line horizontal" />
                    <div className="hamburger-menu-line diagonal-2" />
                  </label>
                  <div id="side-menu-container">
                    <div id="before-side-menu">
                      <span>Content before main menu</span>
                    </div>
                    <nav id="top-menu">
                      <ul id="main-menu">
                        <li className="main-menu-item">
                          <a href="/">Products</a>
                        </li>
                        <li className="main-menu-item">
                          <a href="/create_product">Create Product</a>
                        </li>
                        <li className="main-menu-item">
                          <a href="/update_product/:id">Update Product</a>
                        </li>
                      </ul>
                    </nav>
                    <div></div>
                  </div>
                  <div id="site-header-search">
                    <div id="site-header-search-container">
                      <div className="product-h1">
                        <span>Product Management</span>
                      </div>
                      <form
                        action
                        autoComplete="on"
                        onSubmit={submitingSearchForm}
                      >
                        <input
                          id="search"
                          name="search"
                          type="text"
                          placeholder="Search based on product name ..."
                          onChange={this.changeSearch}
                        />
                        <button
                          id="search_submit"
                          onClick={submitingSearchForm}
                          type="button"
                        >
                          {/*i class="icon-search"*/}
                          <img
                            className="icon-header"
                            src="https://img.icons8.com/ios-glyphs/20/666666/search--v1.png"
                          />
                          {/*/i*/}
                        </button>
                      </form>
                    </div>
                  </div>
                  <div id="site-header-auth-container">
                    <button
                      id="site-header-login-button"
                      className="site-header-auth-button"
                      onClick={this.logoutClick}
                    >
                      <i className="icon-logout">
                        <img
                          className="icon-header"
                          src="https://img.icons8.com/ios-filled/18/666666/login-rounded-right.png"
                        />
                      </i>
                      <span className="span-log">Log out</span>
                    </button>
                  </div>
                </div>
              </header>
            </div>
          );
        }}
      </ProductContext.Consumer>
    );
  }
}

export default withRouter(Header);
