import * as React from "react";
import Avatar from "@mui/material/Avatar";
import Button from "@mui/material/Button";
import CssBaseline from "@mui/material/CssBaseline";
import TextField from "@mui/material/TextField";
import FormControlLabel from "@mui/material/FormControlLabel";
import Checkbox from "@mui/material/Checkbox";
import Link from "@mui/material/Link";
import Grid from "@mui/material/Grid";
import Box from "@mui/material/Box";
import LockOutlinedIcon from "@mui/icons-material/LockOutlined";
import Typography from "@mui/material/Typography";
import Container from "@mui/material/Container";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import { Redirect } from "react-router";
//importing cookies from js-cookie package
import Cookies from "js-cookie";
import "./login.css";

const theme = createTheme();

export default class SignIn extends React.Component {
  state = {
    email: "",
    password: "",
    isLogin: false,
    err_msg: "",
  };
  loginDataFail = (data) => {
    this.setState({ isLogin: true, err_msg: data.status_message });
  };
  changeEmail = (event) => {
    this.setState({ email: event.target.value });
  };
  loginDataSuccess = (data) => {
    Cookies.set("jwt_token", data.jwt_token, {
      expires: 30,
      path: "/",
    });
    const { history } = this.props;
    history.replace("/");
  };
  changePassword = (event) => {
    this.setState({ password: event.target.value });
  };
  handleSubmit = async (event) => {
    event.preventDefault();
    const url = "http://localhost:1947/login";
    const { email, password } = this.state;
    const loginBody = {
      email,
      password,
    };
    const option = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
      body: JSON.stringify(loginBody),
    };
    const respone = await fetch(url, option);
    const data = await respone.json();
    if (data.status_code === 200) {
      this.loginDataSuccess(data);
    } else {
      this.loginDataFail(data);
    }
  };
  render() {
    const { email, password, isLogin, err_msg } = this.state;
    const token = Cookies.get('jwt_token')
    if (token !== undefined) {
      return <Redirect to="/" />
    }
    return (
      <>
        <ThemeProvider theme={theme}>
          <Container component="main" maxWidth="xs">
            <CssBaseline />
            <Box
              sx={{
                marginTop: 8,
                display: "flex",
                flexDirection: "column",
                alignItems: "center",
              }}
            >
              <Avatar sx={{ m: 1, bgcolor: "secondary.main" }}>
                <LockOutlinedIcon />
              </Avatar>
              <Typography component="h1" variant="h5">
                Sign in
              </Typography>
              <Box
                component="form"
                onSubmit={this.handleSubmit}
                noValidate
                sx={{ mt: 1 }}
              >
                <TextField
                  margin="normal"
                  required
                  fullWidth
                  id="email"
                  label="Email Address"
                  name="email"
                  autoComplete="email"
                  autoFocus
                  onChange={this.changeEmail}
                  value={email}
                />
                <TextField
                  margin="normal"
                  required
                  fullWidth
                  name="password"
                  label="Password"
                  type="password"
                  id="password"
                  autoComplete="current-password"
                  onChange={this.changePassword}
                  value={password}
                />
                <FormControlLabel
                  control={<Checkbox value="remember" color="primary" />}
                  label="Remember me"
                />
                <Button
                  type="submit"
                  fullWidth
                  variant="contained"
                  sx={{ mt: 3, mb: 2 }}
                >
                  Sign In
                </Button>
                {isLogin && <p className="para-login">* {err_msg}</p>}
                <Grid sx={{ mb: 8 }} container>
                  <Grid item xs>
                    <Link href="#" variant="body2">
                      Forgot password?
                    </Link>
                  </Grid>
                  <Grid item>
                    <Link href="/signup" variant="body2">
                      {"Don't have an account? Sign Up"}
                    </Link>
                  </Grid>
                </Grid>
              </Box>
            </Box>
          </Container>
        </ThemeProvider>
      </>
    );
  }
}
