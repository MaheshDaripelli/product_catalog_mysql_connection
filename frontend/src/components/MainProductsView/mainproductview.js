import ProductView from "../ProductView/productview";
import ProductContext from "../../Context/context";
import Header from "../Header/header";

const MainProductView = () => {
  return (
    <ProductContext.Consumer>
      {(value) => {
        const { search } = value;
        return (
          <>
            <Header />
            <ProductView search_input={search} />
          </>
        );
      }}
    </ProductContext.Consumer>
  );
};
export default MainProductView;
