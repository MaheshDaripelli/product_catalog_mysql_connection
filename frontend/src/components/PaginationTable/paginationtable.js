import UnstyledTable from "../demo"
import ProductContext from "../../Context/context"

const PaginationTable = ()=>{
    return(
        <ProductContext.Consumer>
            {(value)=>{
                const{search} = value
                return(
                    <UnstyledTable search={search}/>
                )
            }}
        </ProductContext.Consumer>
        
    )
}

export default PaginationTable