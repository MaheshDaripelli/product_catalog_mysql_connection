import { Component } from "react";
import Header from "../Header/header";
import "./updateproduct.css";
import * as React from "react";
import Button from "@mui/material/Button";
import CssBaseline from "@mui/material/CssBaseline";
import TextField from "@mui/material/TextField";
import Box from "@mui/material/Box";
import Container from "@mui/material/Container";
import { createTheme, ThemeProvider } from "@mui/material/styles";
const theme = createTheme();

class UpdateProduct extends Component {
  state = {
    id: "",
    name: "",
    description: "",
    price: "",
    discount: "",
    merchantname: "",
    quantity: "",
    is_update: false,
    err_msg: "",
    is_name: false,
    is_description: false,
    is_price: false,
    is_quantity: false,
    is_merchant: false,
  };

  changeId = (event) => {
    this.setState({ id: event.target.value });
  };

  changeName = (event) => {
    if (event.target.value === "") {
      this.setState({ is_name: true, name: event.target.value });
    } else {
      this.setState({ name: event.target.value, is_name: false });
    }
  };

  changeDescription = (event) => {
    if (event.target.value === "") {
      this.setState({ description: event.target.value, is_description: true });
    } else {
      this.setState({ description: event.target.value, is_description: false });
    }
  };

  changePrice = (event) => {
    if (event.target.value === "") {
      this.setState({ price: event.target.value, is_price: true });
    } else {
      this.setState({ price: event.target.value, is_price: false });
    }
  };

  changeDiscount = (event) => {
    this.setState({ discount: event.target.value });
  };

  merchantName = (event) => {
    if (event.target.value === "") {
      this.setState({ merchantname: event.target.value, is_merchant: true });
    } else {
      this.setState({ merchantname: event.target.value, is_merchant: false });
    }
  };

  changeQuantity = (event) => {
    if (event.target.value === "") {
      this.setState({ quantity: event.target.value, is_quantity: true });
    } else {
      this.setState({ quantity: event.target.value, is_quantity: false });
    }
  };

  submitingUpdatedData = async (event) => {
    event.preventDefault();
    var { id, price, name, description, discount, merchantname, quantity } =
      this.state;
    if (
      id === "" ||
      price === "" ||
      name === "" ||
      description === "" ||
      merchantname === "" ||
      quantity === ""
    ) {
      this.setState({
        is_update: true,
        err_msg: "All fields should be filled",
      });
    } else {
      this.setState({ is_update: false, err_msg: "" });
      const { history } = this.props;
      const url = "http://localhost:1947/update";

      const bodyData = {
        id,
        price,
        name,
        description,
        discount,
        merchantname,
        quantity,
      };
      const option = {
        method: "PUT",
        body: JSON.stringify(bodyData),
        headers: {
          "Content-Type": "application/json",
          "Accept": "application/json",
        },
      };
      try{
      const response = await fetch(url, option);
      const data = await response.json();
      if (data.status_code === 200) {
        this.setState({
          id: "",
          price: "",
        });
        alert("data updated successfully");
        const { history } = this.props;
        history.replace("/");
      }
    }catch(err){
      this.setState({
        err_msg: "Unable to connect to the server Please try again later",
        is_update: true,
      })
    }
    }
  };
  componentDidMount() {
    this.fetchingSingleProduct();
  }

  cancelClicked = () => {
    const { history } = this.props;
    history.replace("/");
  };

  fetchingSingleProduct = async () => {
    const { match } = this.props;
    const { params } = match;
    var { id } = params;
    const url = `http://localhost:1947/product/${id}`;
    const option = {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
    };
    const respone = await fetch(url, option);
    const data = await respone.json();
    if (id !== ":id") {
      this.setState({
        id: id,
        price: data[0].price,
        name: data[0].name,
        description: data[0].description,
        discount: data[0].discount,
        merchantname: data[0].merchant_name,
        quantity: data[0].quantity,
      });
    }
  };

  render() {
    const {
      id,
      price,
      name,
      description,
      discount,
      merchantname,
      quantity,
      is_update,
      err_msg,
      is_description,
      is_merchant,
      is_name,
      is_price,
      is_quantity,
    } = this.state;
    return (
      <>
        <Header />
        <>
          <ThemeProvider theme={theme}>
            <Container component="main" maxWidth="xs">
              <CssBaseline />
              <Box
                sx={{
                  marginTop: 8,
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "center",
                }}
              >
                <Box
                  component="form"
                  onSubmit={this.submitingUpdatedData}
                  noValidate
                  sx={{ mt: 1 }}
                >
                  <TextField
                    margin="normal"
                    required
                    fullWidth
                    label="Id"
                    name="id"
                    autoComplete="id"
                    autoFocus
                    placeholder="id"
                    type="text"
                    onChange={this.changeId}
                    value={id}
                  />
                  <TextField
                    margin="normal"
                    required
                    fullWidth
                    label="Product Name"
                    name="product name"
                    autoComplete="product name"
                    autoFocus
                    placeholder="product name"
                    type="text"
                    value={name}
                    onChange={this.changeName}
                  />
                  {is_name && <p className="para-update">* Required</p>}
                  <TextField
                    margin="normal"
                    required
                    fullWidth
                    name="merchant name"
                    label="Merchant Name"
                    placeholder="merchant name"
                    type="text"
                    onChange={this.merchantName}
                    value={merchantname}
                  />
                  {is_merchant && <p className="para-update">* Required</p>}
                  <TextField
                    margin="normal"
                    required
                    fullWidth
                    label="Description"
                    name="description"
                    autoComplete="description"
                    autoFocus
                    placeholder="description"
                    type="text"
                    onChange={this.changeDescription}
                    value={description}
                  />
                  {is_description && <p className="para-update">* Required</p>}

                  <TextField
                    margin="normal"
                    required
                    fullWidth
                    label="Price"
                    name="price"
                    autoComplete="price"
                    autoFocus
                    placeholder="price"
                    type="number"
                    onChange={this.changePrice}
                    value={price}
                  />
                  {is_price && <p className="para-update">* Required</p>}
                  <TextField
                    margin="normal"
                    fullWidth
                    label="Discount"
                    name="discount"
                    autoComplete="discount"
                    autoFocus
                    placeholder="discount"
                    type="number"
                    onChange={this.changeDiscount}
                    value={discount}
                  />
                  <TextField
                    margin="normal"
                    required
                    fullWidth
                    label="Quantity"
                    name="quantity"
                    autoComplete="quantity"
                    autoFocus
                    placeholder="quantity"
                    type="number"
                    onChange={this.changeQuantity}
                    value={quantity}
                  />
                  {is_quantity && <p className="para-update">* Required</p>}
                  <Button
                    halfWidth
                    variant="contained"
                    color="warning"
                    sx={{ mt: 3, mb: 2, mr: 5 }}
                    onClick={this.cancelClicked}
                  >
                    Cancel
                  </Button>
                  <Button
                    type="submit"
                    halfWidth
                    variant="contained"
                    sx={{ mt: 3, mb: 2 }}
                  >
                    Update
                  </Button>
                  {is_update && <p className="para-update">* {err_msg}</p>}
                </Box>
              </Box>
            </Container>
          </ThemeProvider>
        </>
      </>
    );
  }
}

export default UpdateProduct;
