import { Component } from "react";
import Header from "../Header/header";
import * as React from "react";
import Button from "@mui/material/Button";
import CssBaseline from "@mui/material/CssBaseline";
import TextField from "@mui/material/TextField";
import Paper from "@mui/material/Paper";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import { createTheme, ThemeProvider } from "@mui/material/styles";
const theme = createTheme();

class CreateProduct extends Component {
  state = {
    name: "",
    description: "",
    price: "",
    discount: "",
    merchantname: "",
    quantity: "",
    err_msg: "",
    is_data: false,
    is_name: false,
    is_description: false,
    is_price: false,
    is_quantity: false,
    is_merchant: false,
  };

  changeName = (event) => {
    if (event.target.value === "") {
      this.setState({ is_name: true, name: event.target.value });
    } else {
      this.setState({ name: event.target.value, is_name: false });
    }
  };

  changeDescription = (event) => {
    if (event.target.value === "") {
      this.setState({ description: event.target.value, is_description: true });
    } else {
      this.setState({ description: event.target.value, is_description: false });
    }
  };

  changePrice = (event) => {
    if (event.target.value === "") {
      this.setState({ price: event.target.value, is_price: true });
    } else {
      this.setState({ price: event.target.value, is_price: false });
    }
  };

  changeDiscount = (event) => {
    this.setState({ discount: event.target.value });
  };

  merchantName = (event) => {
    if (event.target.value === "" || !/^[a-zA-Z' ]+$/.test(event.target.value)) {
      this.setState({ merchantname: event.target.value, is_merchant: true });
    } else {
      this.setState({ merchantname: event.target.value, is_merchant: false });
    }
  };

  changeQuantity = (event) => {
    if (event.target.value === "") {
      this.setState({ quantity: event.target.value, is_quantity: true });
    } else {
      this.setState({ quantity: event.target.value, is_quantity: false });
    }
  };

  submitingUserData = async (event) => {
    event.preventDefault();
    const url = "http://localhost:1947/product/create";
    const { name, description, price, quantity, merchantname, discount } =
      this.state;
    const bodyData = {
      name,
      description,
      price,
      quantity,
      merchant_name: merchantname,
      discount,
    };
    const option = {
      method: "POST",
      body: JSON.stringify(bodyData),
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
    };
    try{
      const response = await fetch(url, option);
    const data = await response.json();
    if (data.status_code === 200) {
      this.setState({
        name: "",
        description: "",
        merchantname: "",
        discount: "",
        price: "",
        quantity: "",
        err_msg: "",
        is_data: false,
      });
      alert("data added successfully");
      const { history } = this.props;
      history.replace("/");
    } else {
      this.setState({ err_msg: data.status_message, is_data: true });
    }
    }catch(err){
      this.setState({
        err_msg: "Unable to connect to the server Please try again later",
        is_data: true,
      })
    }
    
  };

  render() {
    const {
      is_data,
      err_msg,
      is_name,
      is_description,
      is_merchant,
      is_price,
      is_quantity,
    } = this.state;
    return (
      <>
        <Header />
        <ThemeProvider theme={theme}>
          <Grid container component="main" sx={{ height: "100vh" }}>
            <CssBaseline />
            <Grid
              item
              xs={false}
              sm={4}
              md={7}
              sx={{
                backgroundImage: "url(https://source.unsplash.com/random)",
                backgroundRepeat: "no-repeat",
                backgroundColor: (t) =>
                  t.palette.mode === "light"
                    ? t.palette.grey[50]
                    : t.palette.grey[900],
                backgroundSize: "cover",
                backgroundPosition: "center",
              }}
            />
            <Grid
              item
              xs={12}
              sm={8}
              md={5}
              component={Paper}
              elevation={6}
              square
            >
              <Box
                sx={{
                  my: 8,
                  mx: 4,
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "center",
                }}
              >
                <Box
                  component="form"
                  noValidate
                  onSubmit={this.submitingUserData}
                  sx={{ mt: 1 }}
                >
                  <TextField
                    margin="normal"
                    required
                    fullWidth
                    label="Product Name"
                    name="product name"
                    autoComplete="product name"
                    autoFocus
                    placeholder="product name"
                    type="text"
                    onChange={this.changeName}
                  />
                  {is_name && <p className="para-update">* Required</p>}
                  <TextField
                    margin="normal"
                    required
                    fullWidth
                    name="merchant name"
                    label="Merchant Name"
                    placeholder="merchant name"
                    type="text"
                    onChange={this.merchantName}
                  />
                  {is_merchant && <p className="para-update">* Required and Merchant name should have only alphabets</p>}
                  <TextField
                    margin="normal"
                    required
                    fullWidth
                    label="Description"
                    name="description"
                    autoComplete="description"
                    autoFocus
                    placeholder="description"
                    type="text"
                    onChange={this.changeDescription}
                  />
                  {is_description && <p className="para-update">* Required</p>}
                  <TextField
                    margin="normal"
                    required
                    fullWidth
                    label="Price"
                    name="price"
                    autoComplete="price"
                    autoFocus
                    placeholder="price"
                    type="number"
                    onChange={this.changePrice}
                  />
                  {is_price && <p className="para-update">* Required</p>}
                  <TextField
                    margin="normal"
                    fullWidth
                    label="Discount"
                    name="discount"
                    autoComplete="discount"
                    autoFocus
                    placeholder="discount"
                    type="number"
                    onChange={this.changeDiscount}
                  />
                  <TextField
                    margin="normal"
                    required
                    fullWidth
                    label="Quantity"
                    name="quantity"
                    autoComplete="quantity"
                    autoFocus
                    placeholder="quantity"
                    type="number"
                    onChange={this.changeQuantity}
                  />
                  {is_quantity && <p className="para-update">* Required</p>}
                  <Button
                    type="submit"
                    fullWidth
                    variant="contained"
                    sx={{ mt: 3, mb: 2 }}
                  >
                    Create
                  </Button>
                  {is_data && <p className="para-update">* {err_msg}</p>}
                </Box>
              </Box>
            </Grid>
          </Grid>
        </ThemeProvider>
      </>
    );
  }
}

export default CreateProduct;
