import Popup from "reactjs-popup";
import { Link, Redirect, withRouter } from "react-router-dom";
import { Component } from "react";
import "./product.css";
import AlertDialogSlide from "../Popup/popup";

class Product extends Component {
  deleteApiCall = async () => {
    const { each } = this.props;
    const url = "http://localhost:1947/product/delete";
    const bodyData = {
      id: each.id,
    };
    const option = {
      method: "DELETE",
      body: JSON.stringify(bodyData),
      headers: {
        "Content-Type": "application/json",
        "Accept": "application/json",
      },
    };
      const response = await fetch(url, option);
      const data = await response.json();
      // if (data.status_code === 200) {
      
      // }
  };
  render() {
    const { each } = this.props;
    return (
      <div className="data-row og-row og-li Experienced Engineering 7.3 Ready to hire Andhra Pradesh Yes">
        <div className="og-li-col og-li-col-3 text">{each.name}</div>
        <div className="og-li-col og-li-col-3 text-center text">
          {each.description}
        </div>
        <div className="og-li-col og-li-col-3 text-center text">
          Rs {each.price}/-
        </div>
        <div className="og-li-col og-li-col-3 text-center text">
          Rs {each.discount}/-
        </div>
        <div className="og-li-col og-li-col-3 text-center text">
          {each.merchant_name}
        </div>
        <div className="og-li-col og-li-col-3 text-center text">
          {each.quantity}
        </div>
        <div className="flex og-li-col og-li-col-3">
          <Link
            to={{
              pathname: `/update_product/${each.id}`,
              query: each,
            }}
          >
            <button className="update-button">Update</button>
          </Link>
          <AlertDialogSlide callback = {this.deleteApiCall}/>
          {/* <button className="delete-button" onClick={this.deleteApiCall}>
            Delete
          </button> */}
        </div>
      </div>
    );
  }
}

export default withRouter(Product);
