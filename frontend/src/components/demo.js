import * as React from "react";
import { styled } from "@mui/system";
import TablePaginationUnstyled from "@mui/base/TablePaginationUnstyled";
import Header from "../components/Header/header";
import "./demo.css";
import AlertDialogSlide from "./Popup/popup";
import { Link } from "react-router-dom";

function createData(name, calories, fat) {
  return { name, calories, fat };
}

const rows = [
  { name: "Cupcake", calories: 305, fat: 3.7 },
  createData("Donut", 452, 25.0),
  createData("Eclair", 262, 16.0),
  createData("Frozen yoghurt", 159, 6.0),
  createData("Gingerbread", 356, 16.0),
  createData("Honeycomb", 408, 3.2),
  createData("Ice cream sandwich", 237, 9.0),
  createData("Jelly Bean", 375, 0.0),
  createData("KitKat", 518, 26.0),
  createData("Lollipop", 392, 0.2),
  createData("Marshmallow", 318, 0),
  createData("Nougat", 360, 19.0),
  createData("Oreo", 437, 18.0),
];
//.sort((a, b) => (a.calories < b.calories ? -1 : 1));

const Root = styled("div")`
  table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
  }

  td,
  th {
    border: 1px solid #ddd;
    text-align: left;
    padding: 8px;
  }

  th {
    background-color: #ddd;
  }
`;

const CustomTablePagination = styled(TablePaginationUnstyled)`
  & .MuiTablePaginationUnstyled-toolbar {
    display: flex;
    flex-direction: column;
    align-items: flex-start;
    gap: 10px;

    @media (min-width: 768px) {
      flex-direction: row;
      align-items: center;
    }
  }

  & .MuiTablePaginationUnstyled-selectLabel {
    margin: 0;
  }

  & .MuiTablePaginationUnstyled-displayedRows {
    margin: 0;

    @media (min-width: 768px) {
      margin-left: auto;
    }
  }

  & .MuiTablePaginationUnstyled-spacer {
    display: none;
  }

  & .MuiTablePaginationUnstyled-actions {
    display: flex;
    gap: 0.25rem;
  }
`;

export default class UnstyledTable extends React.Component {
  // const [page, setPage] = React.useState(0);
  // const [rowsPerPage, setRowsPerPage] = React.useState(5);
  state = { page: 0, rowsPerPage: 5, rows: [], order_by: "" };
  componentDidMount() {
    this.productApiCall();
  }

  changeSelect = (event) => {
    this.setState({ order_by: event.target.value }, this.productApiCall);
  };

  deleteApiCall = async () => {
    const { each } = this.props;
    const url = "http://localhost:1947/product/delete";
    const bodyData = {
      id: each.id,
    };
    const option = {
      method: "DELETE",
      body: JSON.stringify(bodyData),
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
    };
    const response = await fetch(url, option);
    const data = await response.json();
    // if (data.status_code === 200) {

    // }
  };

  componentDidUpdate() {
    this.productApiCall();
  }

  productApiCall = async () => {
    const { order_by } = this.state;
    const { search } = this.props;
    // const search = localStorage.getItem("search")
    const url = "http://localhost:1947/products";
    const option = {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        "Accept": "application/json",
        order: order_by,
        search: search,
      },
    };
    try {
      const response = await fetch(url, option);
      console.log(response)
      if (response.status === 200) {
        const data = await response.json();
        console.log(data)
        this.setState({
          rows: data,
          //apiStatus: apiStatusConstants.success,
        });
      } else {
        //this.setState({ apiStatus: apiStatusConstants.failure });
      }
    } catch (err) {
      // this.setState({ apiStatus: apiStatusConstants.serverdown });
    }
  };
  // Avoid a layout jump when reaching the last page with empty rows.
  emptyRows =
    this.state.page > 0
      ? Math.max(
          0,
          (1 + this.state.page) * this.state.rowsPerPage - rows.length
        )
      : 0;

  handleChangePage = (event, newPage) => {
    this.setState({ page: newPage });
  };

  handleChangeRowsPerPage = (event) => {
    // setRowsPerPage(parseInt(event.target.value, 10));
    this.setState({ page: 0, rowsPerPage: parseInt(event.target.value) });
    // setPage(0);
  };
  render() {
    const { page, rowsPerPage, rows } = this.state;
    console.log(rows);
    return (
      <>
        <Header />
        <div className="pagintion-table">
          <div className="section">
            <select onChange={this.changeSelect}>
              <option value="ASC">Price (Low - High)</option>
              <option value="DESC">Price (High - Low)</option>
            </select>
          </div>
          <Root sx={{ maxWidth: "80%" }}>
            <table aria-label="custom pagination table">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Description</th>
                  <th>Price</th>
                  <th>Discount</th>
                  <th>Merchant Name</th>
                  <th>Quantity</th>
                  <th>Update Operation</th>
                  <th>Delete Operation</th>
                </tr>
              </thead>
              <tbody>
                {(rowsPerPage > 0
                  ? rows.slice(
                      page * rowsPerPage,
                      page * rowsPerPage + rowsPerPage
                    )
                  : rows
                ).map((row) => (
                  <tr key={row.name}>
                    <td>{row.name}</td>
                    <td style={{ width: 160 }} align="right">
                      {row.description}
                    </td>
                    <td style={{ width: 160 }} align="right">
                      {row.price}
                    </td>
                    <td style={{ width: 160 }} align="right">
                      {row.discount}
                    </td>
                    <td style={{ width: 160 }} align="right">
                      {row.merchant_name}
                    </td>
                    <td style={{ width: 160 }} align="right">
                      {row.quantity}
                    </td>
                    <td style={{ width: 160 }} align="right">
                      <Link
                        to={{
                          pathname: `/update_product/${row.id}`,
                          query: row,
                        }}
                      >
                        <button className="update-button">Update</button>
                      </Link>
                    </td>
                    <td style={{ width: 160 }} align="right">
                      <AlertDialogSlide callback={this.deleteApiCall} />
                    </td>
                  </tr>
                ))}

                {this.emptyRows > 0 && (
                  <tr style={{ height: 41 * this.emptyRows }}>
                    <td colSpan={3} />
                  </tr>
                )}
              </tbody>
              <tfoot>
                <tr>
                  <CustomTablePagination
                    rowsPerPageOptions={[
                      5,
                      10,
                      25,
                      { label: "All", value: -1 },
                    ]}
                    colSpan={3}
                    count={rows.length}
                    rowsPerPage={rowsPerPage}
                    page={page}
                    componentsProps={{
                      select: {
                        "aria-label": "rows per page",
                      },
                      actions: {
                        showFirstButton: true,
                        showLastButton: true,
                      },
                    }}
                    onPageChange={this.handleChangePage}
                    onRowsPerPageChange={this.handleChangeRowsPerPage}
                  />
                </tr>
              </tfoot>
            </table>
          </Root>
        </div>
      </>
    );
  }
}
