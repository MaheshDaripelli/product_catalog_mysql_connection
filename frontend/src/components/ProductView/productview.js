import { Component } from "react";
import Product from "../Product/product";
import "./productview.css";
import { AiOutlineRight } from "react-icons/ai";
import { AiOutlineLeft } from "react-icons/ai";

const apiStatusConstants = {
  initial: "INITIAL",
  success: "SUCCESS",
  failure: "FAILURE",
  serverdown: "SERVER DOWN",
};

class ProductView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      productData: [],
      order_by: "",
      apiStatus: apiStatusConstants.initial,
      limit: 5,
      activePage: 1,
      dataLength:"",
      dataCount: ""
    };
  }
  componentDidUpdate() {
    this.productApiCall();
  }

  onClickLeftArrow = () => {
    const { activePage } = this.state;
    if (activePage > 1) {
      this.setState(
        (prevState) => ({
          activePage: prevState.activePage - 1,
        }),
        this.productApiCall
      );
    }
  };

  onClickRightArrow = () => {
    const { activePage,dataLength } = this.state;
    if (activePage < dataLength) {
      this.setState(
        (prevState) => ({
          activePage: prevState.activePage + 1,
        }),
        this.productApiCall
      );
    }
  };

  countApiCall = async()=>{
    const {limit} = this.state
    const url = "http://localhost:1947/allproducts"
    const option = {
      method:"GET",
      headers:{
        "Content-Type": "application/json",
        "Accept": "application/json",
      }
    }
    const response = await fetch(url,option)
    const data = await response.json()
    console.log(9%5)
    this.setState({dataLength:Math.ceil(data.length/limit),dataCount:data.length})
  }

  componentDidMount() {
    this.productApiCall();
    this.countApiCall()
  }

  selectLimitChange = (event)=>{
    const{dataCount} = this.state
    this.setState({limit:event.target.value,dataLength:Math.ceil(dataCount/event.target.value),activePage:1},this.productApiCall)
  }

  changeSelect = (event) => {
    this.setState({ order_by: event.target.value }, this.productApiCall);
  };

  renderProductFailureView = () => (
    <div className="videosDiv1">
      <img
        src="https://assets.ccbp.in/frontend/react-js/nxt-watch-failure-view-light-theme-img.png"
        alt="failure view"
        className="img10"
      />
      <h1>Oops! Something Went Wrong</h1>
      <p>unable to connect to the server</p>
      <button type="button" className="button6" onClick={this.clickingRetry}>
        Retry
      </button>
    </div>
  );

  renderProducts = () => {
    const { productData,activePage,dataLength } = this.state;
    return (
      <>
        <div className="og-contianer">
          <div className="section">
            <select onChange={this.changeSelect}>
              <option value="ASC">Price (Low - High)</option>
              <option value="DESC">Price (High - Low)</option>
            </select>
          </div>
          <div className="og-row og-li og-li-head text">
            <div className="og-li-col og-li-col-3">Name</div>
            <div className="og-li-col og-li-col-3 text-center text">
              Description
            </div>
            <div className="og-li-col og-li-col-3 text-center text">Price</div>
            <div className="og-li-col og-li-col-3 text-center text">
              Discount
            </div>
            <div className="og-li-col og-li-col-3 text-center text">
              Merchant Name
            </div>
            <div className="og-li-col og-li-col-3 text-center text">
              Quantity
            </div>
            <div className="og-li-col og-li-col-3 text-center text">
              Operations
            </div>
          </div>
          {productData.map((each) => (
            <Product each={each} key={each.id} />
          ))}
          <div className="arrows">
            <span className="text text1">Rows Per Page</span>
            <select className="select-limit" onChange={this.selectLimitChange}>
              <option value="5">5</option>
              <option value="10">10</option>
              <option value="15">15</option>
              <option value="20">20</option>
            </select>
            <div className="arrows-div">
            <AiOutlineLeft className="arr" onClick={this.onClickLeftArrow} />
            <span className="text text2">{activePage} to  of {dataLength}</span>
            <AiOutlineRight className="arr" onClick={this.onClickRightArrow} />
            </div>
          </div>
        </div>
      </>
    );
  };

  functionRendering = () => {
    const { apiStatus } = this.state;
    switch (apiStatus) {
      case apiStatusConstants.success:
        return this.renderProducts();
      case apiStatusConstants.failure:
        return this.renderProductFailureView();
      case apiStatusConstants.serverdown:
        return this.renderProductFailureView();
      default:
        return null;
    }
  };

  productApiCall = async () => {
    const { order_by } = this.state;
    const { search_input } = this.props;
    const{activePage,limit} = this.state
    const offset = (activePage - 1) * limit
    // const search = localStorage.getItem("search")
    const url = "http://localhost:1947/products"
    const option = {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        "Accept": "application/json",
        order: order_by,
        search: search_input,
        limit,
        offset,
        checker:"pagination contains"
      },
    };
    try {
      const response = await fetch(url, option);
      if (response.status === 200) {
        const data = await response.json();
        this.setState({
          productData: data,
          apiStatus: apiStatusConstants.success,
        });
      } else {
        this.setState({ apiStatus: apiStatusConstants.failure });
      }
    } catch (err) {
      this.setState({ apiStatus: apiStatusConstants.serverdown });
    }
  };
  render() {
    return this.functionRendering();
  }
}
export default ProductView;
