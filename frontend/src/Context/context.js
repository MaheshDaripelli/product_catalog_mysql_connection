import React from "react";

const ProductContext = React.createContext({
  search: "",
  updateSearch: () => {},
});

export default ProductContext;
