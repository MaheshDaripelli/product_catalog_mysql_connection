import "./App.css";
import { Route, Switch, BrowserRouter } from "react-router-dom";
import CreateProduct from "./components/CreateProduct/createproduct";
import UpdateProduct from "./components/UpdateProduct/updateproduct";
import DeleteProduct from "./components/DeleteProduct/deleteproduct";
import ProductContext from "./Context/context";
import { Component } from "react";
import MainProductView from "./components/MainProductsView/mainproductview";
import SignIn from "./components/Login/login";
import SignUp from "./components/SignUp/signup";
import ProtectedRoute from "./components/ProtectedRoute";
import UnstyledTable from "./components/demo";
import PaginationTable from "./components/PaginationTable/paginationtable";

class App extends Component {
  state = {
    search: "",
  };
  updateSearch = (id) => {
    this.setState({ search: id });
    localStorage.setItem("search", id);
    // document.location.reload(false)
  };
  render() {
    const { search } = this.state;
    return (
      <ProductContext.Provider
        value={{
          search,
          updateSearch: this.updateSearch,
        }}
      >
        <div>
          <div className="main-div-app">
            <BrowserRouter>
              <Switch>
                <ProtectedRoute exact path="/" component={MainProductView} />
                <ProtectedRoute
                  exact
                  path="/create_product"
                  component={CreateProduct}
                />
                <ProtectedRoute
                  exact
                  path="/update_product/:id"
                  component={UpdateProduct}
                />
                <ProtectedRoute
                  exact
                  path="/delete_product/:id"
                  component={DeleteProduct}
                />
                <Route exact path="/signin" component={SignIn} />
                <Route exact path="/signup" component={SignUp} />
                <Route exact path="/table" component={PaginationTable} />
              </Switch>
            </BrowserRouter>
          </div>
        </div>
      </ProductContext.Provider>
    );
  }
}

export default App;
